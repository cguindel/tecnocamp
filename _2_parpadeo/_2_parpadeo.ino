/*
  Blink
  Enciende y apaga el LED de la placa durante un segundo */

// La funcion setup() se llama una vez al hacer reset o encender
void setup() {
  // Inicializa el pin 13 como salida
  pinMode(13, OUTPUT);
}

// La función loop() se ejecuta cíclicamente sin parar
void loop() {
  digitalWrite(13, HIGH);   // Enciende el LED (HIGH es el nivel de voltaje)
  delay(1000);              // Espera un segundo
  digitalWrite(13, LOW);    // Apaga el led
  delay(1000);              // Espera otro segundo
}
