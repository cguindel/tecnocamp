#include <Ultrasonic.h>

int IZQ_ECHO_PIN = /** Escribir pin echo izq. **/;
int IZQ_TRIG_PIN = /** Escribir pin trigger izq. **/;

Ultrasonic ultraizq(IZQ_TRIG_PIN, IZQ_ECHO_PIN); 

int medida_izq = 0;

void setup() {
  // Se ejecuta una vez
  Serial.begin(9600);
}

void loop() {
  // Se ejecuta continuamente
  
  medida_izq = ultraizq.Timing(); /** Modificar para calcular la distancia **/
  
  Serial.print("Izquierda: ");
  Serial.print(medida_izq); 
  Serial.println(" us"); /** Poner unidades correctas **/           
  delay(100); //Lo hacemos 10 veces por segundo
  
  /** Leer y mostrar ultrasonido derecha **/
}
