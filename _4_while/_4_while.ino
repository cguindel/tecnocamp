/*
  Blink
  Enciende y apaga el LED de la placa condicionado a un valor */
// Variables
int numero_uno = 0;
int numero_dos = 0;
int suma = 0;

// La funcion setup() se llama una vez al hacer reset o encender
void setup() {
  // Inicializa el pin 13 como salida
  pinMode(13, OUTPUT);
  suma = numero_uno + numero_dos;
}

// La función loop() se ejecuta cíclicamente sin parar
void loop() {
  while (suma > 0)
  {
    digitalWrite(13, HIGH);   // Enciende el LED (HIGH es el nivel de voltaje)
    delay(1000);
    suma = suma - 1;
  }
  digitalWrite(13, LOW);    // Apaga el led
  delay(1000);
}
