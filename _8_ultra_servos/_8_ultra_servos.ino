#include <Servo.h>
#include <Ultrasonic.h>

int IZQ_ECHO_PIN = /** Escribir pin echo izq. **/;
int IZQ_TRIG_PIN = /** Escribir pin trigger izq. **/;

int DER_ECHO_PIN = /** Escribir pin echo der. **/;
int DER_TRIG_PIN = /** Escribir pin trigger der. **/;

/** Añadir pines para los servos **/

Ultrasonic ultraizq(IZQ_TRIG_PIN, IZQ_ECHO_PIN); 
Ultrasonic ultrader(DER_TRIG_PIN, DER_ECHO_PIN);

/** Añadir servos **/

/** Añadir funciones para los servos **/

int dist_izq = 0;
int dist_der = 0;

void setup() {
  // Se ejecuta una vez
  /** Unir servos a los pines correspondientes **/
}

void loop() {
  // Se ejecuta continuamente
  
  // Aquí pedimos las distancias y las guardamos en dist_l y dist_r
  dist_izq = ultraizq.Ranging(CM);
  dist_der = ultrader.Ranging(CM);

  // Usar los motores
  /** Si hay un objeto a la derecha,
  /*    girar a la izquierda 
   *  si no, si hay un objeto a la izquierda
   *    girar a la derecha
   *  si no,
   *    ir recto
   *  etc...  **/

  delay(100); //Lo hacemos cada segundo
}
