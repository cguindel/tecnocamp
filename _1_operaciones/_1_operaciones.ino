// Variables
int numero_uno = 0;
int numero_dos = 0;
int suma = 0;
int producto = 0;
int cociente = 0;
int resto = 0;

// Se ejecuta setup() una vez al hacer reset o encender
void setup() {
  // Inicializa la comunicacion serie a 9600 bits por segundo:
  Serial.begin(9600);
}

// La función loop() es ciclica y nunca termina
void loop() {
  // Muestra por el monitor serie la suma
  suma = numero_uno + numero_dos;
  Serial.print("Suma: ");
  Serial.println(suma);
  delay(3000);        // Espera 3 segundos
  producto = numero_uno * numero_dos;
  Serial.print("Producto: ");
  Serial.println(producto);
  delay(3000);        // Espera 3 segundos
}



