/*
  Funciones
  Enciende y apaga el LED mediante funciones */

// Enciende el led del pin 13 durante "tiempo" en segundos
void encenderLED(float tiempo)
{
  digitalWrite(13, HIGH);
  delay(tiempo * 1000);
  digitalWrite(13, LOW);
}

// La funcion setup() se llama una vez al hacer reset o encender
void setup() {
  // Inicializa el pin 13 como salida
  pinMode(13, OUTPUT);
}

// La función loop() se ejecuta cíclicamente sin parar
void loop() {
  // Eciende el LED durante 5 segundos
  encenderLED(5);
  // Espera sin hacer nada durante 3 segundos
  delay(3000);
}
